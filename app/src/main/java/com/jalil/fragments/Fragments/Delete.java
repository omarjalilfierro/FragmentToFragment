package com.jalil.fragments.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jalil.fragments.R;

/**
 * Created by Jalil on 14/03/2018.
 */

public class Delete extends Fragment {

    final static String DATA_RECEIVE = "data_receive";
    TextView showReceivedData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.delete, container, false);
        showReceivedData = (TextView) view.findViewById(R.id.showReceivedData);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = getArguments();
        if (args != null) {
            showReceivedData.setText(args.getString(DATA_RECEIVE));
        }
    }
}
