package com.jalil.fragments.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jalil.fragments.R;

/**
 * Created by Jalil on 12/03/2018.
 */

public class Add extends Fragment {

    mInterface callBackActivity;
    Bundle bundle = new Bundle();

    private EditText name, lastName, password;

    private Button btnAcc;

    public Add() {
    }

    @Nullable //Starts when we use the constructor
    @Override //Here we inflate layouts and change the widgets
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.layout_add,container,false);

        btnAcc = root.findViewById(R.id.btnAccept);
        bundle.putString("key","abc");

        Delete del = new Delete();
        del.setArguments(bundle);

        btnAcc.setOnClickListener(view -> {getActivity()
                .getFragmentManager()
                .beginTransaction()
                .replace(R.id.container,del)
                .commit();
                callBackActivity.passData("Si jala morro");
        });

        return root;
    }

    public interface mInterface{
        public void getData(String name, String lastName, String password);
        public void msgError(int i);
        public void passData(String data);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //callBackActivity.passData("Si jalo");
    }

    @Override
    public void onAttach(Context context) { //Creates callback if we want to return a value to the activity that is calling
        super.onAttach(context);
        try {
            callBackActivity = (mInterface) getActivity();
        }catch (ClassCastException e){
            e.printStackTrace();
        }
    }
}
