package com.jalil.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.jalil.fragments.Fragments.Add;
import com.jalil.fragments.Fragments.Delete;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Add.mInterface {

    private Button add, delete, show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        add = findViewById(R.id.btnAdd);
        add.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (view.getId()){
            case R.id.btnAdd:
                fragmentTransaction.replace(R.id.container, new Add()).commit();
                getSupportActionBar().setTitle("Add");
                break;
        }

    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void getData(String name, String lastName, String password) {

    }

    @Override
    public void msgError(int i) {

    }

    @Override
    public void passData(String data) {
        Delete fragmentDelete = new Delete();
        Bundle args = new Bundle();
        args.putString("data_receive", "Holiwi si me envie");
        fragmentDelete.setArguments(args);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragmentDelete )
                .commit();
    }
}
